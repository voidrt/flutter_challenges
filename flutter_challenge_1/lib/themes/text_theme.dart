import 'package:flutter/material.dart';
import 'color_theme.dart';

class TextStyles {
  static const TextStyle title = TextStyle(
    fontSize: FontSize.defaultTitle,
    fontWeight: FontWeight.w800,
    color: ColorPalette.black,
  );

  static const TextStyle subtitle = TextStyle(
    fontSize: FontSize.defaultBodySize,
    fontWeight: FontWeight.w500,
    color: ColorPalette.black,
  );

  static const TextStyle cardSubtitle = TextStyle(
    fontSize: FontSize.defaultBodySize,
    fontWeight: FontWeight.w500,
    color: ColorPalette.black,
  );

  static const TextStyle cardTitle = TextStyle(
    fontSize: FontSize.g,
    fontWeight: FontWeight.bold,
    color: ColorPalette.black,
  );

  static const TextStyle bodyText1 = TextStyle(
    fontSize: FontSize.defaultTitle,
    fontWeight: FontWeight.w800,
    color: ColorPalette.black,
  );

  static const TextStyle button = TextStyle(
    fontSize: FontSize.defaultTitle,
    fontWeight: FontWeight.w700,
    color: ColorPalette.pureWhite,
  );
}

class FontSize {
  static const double p = 8;
  static const double m = 12;
  static const double defaultBodySize = 16;
  static const double defaultTitle = 24;
  static const double g = 28;
  static const double gg = 32;
  static const double exg = 46;
}
