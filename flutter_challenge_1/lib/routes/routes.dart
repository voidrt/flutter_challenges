import 'package:flutter_challenge_1/screens/home/home.dart';
import 'package:get/get.dart';
//--------------------------------------------------------

class GetRoutes {
  static const String home = '/';

  static List<GetPage> routes = [
    GetPage(
      name: home,
      page: () => const Home(),
    ),
  ];
}
