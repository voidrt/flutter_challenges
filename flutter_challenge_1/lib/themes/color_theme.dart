import 'package:flutter/material.dart';

class ColorPalette {
  static const primary = Color.fromARGB(255, 140, 41, 134);
  static const primaryDark = Color.fromARGB(255, 104, 44, 140);
  static const secondary = Color.fromARGB(255, 237, 30, 121);
  static const black = Color(0xff222222);
  static const neutral50 = pureWhite;
  static const pureWhite = Color.fromARGB(255, 255, 255, 255);
  static const iconColor = Color(0xFFecedee);
  static const grey = Color.fromARGB(255, 150, 150, 150);
  static const darkGrey = Color.fromARGB(255, 52, 54, 53);
  static const darkThemeBackground = Color.fromARGB(255, 20, 21, 36);
  static const background = Color.fromARGB(255, 28, 33, 46);
  static const gradientBright = secondary;
  static const gradientMiddle = primary;
  static const gradientDark = primaryDark;
}
