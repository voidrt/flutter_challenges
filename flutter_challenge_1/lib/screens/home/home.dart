import 'package:flutter/material.dart';

import '../../widgets/book_grid_view.dart/book_grid.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return BookGridView(
            constraints: constraints,
          );
        },
      ),
    );
  }
}
