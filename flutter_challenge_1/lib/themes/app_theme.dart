import 'package:flutter/material.dart';
import 'color_theme.dart';
import 'text_theme.dart';

class AppTheme {
  static var appTheme = ThemeData().copyWith(
    useMaterial3: true,
    primaryColor: ColorPalette.primary,
    primaryColorDark: ColorPalette.primaryDark,
    colorScheme: const ColorScheme.dark().copyWith(
      brightness: Brightness.dark,
      primary: ColorPalette.primaryDark,
      onPrimary: ColorPalette.primary,
      secondary: ColorPalette.secondary,
    ),
    scaffoldBackgroundColor: ColorPalette.background,
    unselectedWidgetColor: Colors.white.withOpacity(0.67),
    appBarTheme: const AppBarTheme(
      centerTitle: true,
      elevation: 3,
      toolbarHeight: 50,
    ),
    inputDecorationTheme: const InputDecorationTheme().copyWith(
      focusColor: ColorPalette.black,
      labelStyle: const TextStyle(
        fontWeight: FontWeight.w400,
        color: ColorPalette.black,
      ),
    ),
    textTheme: const TextTheme().copyWith(
      bodyText1: TextStyles.cardTitle,
      bodyText2: TextStyles.cardSubtitle,
      button: TextStyles.button,
      subtitle1: TextStyles.subtitle,
      headline6: TextStyles.title,
    ),
  );
}
