import 'dart:convert';

class BookModel {
  BookModel({
    required this.id,
    required this.title,
    required this.authors,
    required this.publisher,
    required this.publishedDate,
    required this.description,
    required this.language,
    required this.smallThumbnail,
    required this.thumbnail,
    required this.previewLink,
    required this.infoLink,
    required this.canonicalVolumeLink,
  });

  String id;
  String title;
  List<String> authors;
  String publisher;
  String publishedDate;
  String description;
  String language;
  String smallThumbnail;
  String thumbnail;
  String previewLink;
  String infoLink;
  String canonicalVolumeLink;

  factory BookModel.fromMap(Map<String, dynamic> map) {
    return BookModel(
      id: map['id'] ?? '',
      title: map['title'] ?? '',
      authors:
          map["authors"] == null ? [] : List.from(map["authors"].map((x) => x)),
      publisher: map['publisher'] ?? '',
      publishedDate: map['publishedDate'] ?? '',
      description: map['description'] ?? '',
      language: map['language'] ?? '',
      smallThumbnail: map['smallThumbnail'] ?? '',
      thumbnail: map['thumbnail'] ?? '',
      previewLink: map['previewLink'] ?? '',
      infoLink: map['infoLink'] ?? '',
      canonicalVolumeLink: map['canonicalVolumeLink'] ?? '',
    );
  }

  factory BookModel.fromJson(String json) =>
      BookModel.fromMap(jsonDecode(json));
}
