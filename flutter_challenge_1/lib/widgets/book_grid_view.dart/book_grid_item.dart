import 'package:flutter/material.dart';

import '../../models/book_models.dart';
import '../../themes/color_theme.dart';
import '../../themes/padding.dart';

class BookGridItem extends StatelessWidget {
  const BookGridItem({
    Key? key,
    required this.child,
    this.opacity = 1,
  }) : super(key: key);

  final double opacity;
  final BookModel child;

  @override
  Widget build(BuildContext context) {
    final MediaQueryData mediaQuery = MediaQuery.of(context);

    return SizedBox(
      height: mediaQuery.size.height,
      width: mediaQuery.size.width * 0.10,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 4,
        surfaceTintColor: ColorPalette.black.withOpacity(opacity),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              height: mediaQuery.size.height * 0.17,
              child: FittedBox(
                fit: BoxFit.fitHeight,
                child: Image.network(child.smallThumbnail),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: PaddingMeasure.pp),
              child: Flexible(
                child: Text(
                  child.title,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: ColorPalette.pureWhite,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
