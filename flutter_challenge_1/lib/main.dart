import 'package:flutter/material.dart';
import 'package:get/get.dart';
//---------------------------------------------------------
import './bindings_and_controllers/books_controller.dart';
import 'package:flutter_challenge_1/repositories/library_stock.dart';
import 'package:flutter_challenge_1/themes/app_theme.dart';
import 'package:flutter_challenge_1/routes/routes.dart';

void main() {
  Get.lazyPut(() => GenerateLibraryStock());
  Get.lazyPut(() => BookController(Get.find()));
  runApp(const MaterialApp());
}

class MaterialApp extends StatelessWidget {
  const MaterialApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      getPages: GetRoutes.routes,
      initialRoute: GetRoutes.home,
      theme: AppTheme.appTheme,
    );
  }
}
