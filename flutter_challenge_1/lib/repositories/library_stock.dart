import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_challenge_1/models/book_models.dart';
import 'package:flutter_challenge_1/repositories/book_repo.dart';

class GenerateLibraryStock implements LibraryStock {
  @override
  Future<List<BookModel>> fetchLibraryStock() async {
    final String jsonBooks =
        await rootBundle.loadString('lib/assets/flutter-books.json');
    final List<dynamic> booksList = jsonDecode(jsonBooks);

    return booksList.map<BookModel>((book) => BookModel.fromMap(book)).toList();
  }
}
