import 'package:flutter/material.dart';
import 'package:flutter_challenge_1/widgets/book_grid_view.dart/book_grid_item.dart';

import '../../models/book_models.dart';

class BookItem extends StatelessWidget {
  const BookItem({
    Key? key,
    required this.book,
  }) : super(key: key);

  final BookModel book;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 0.4),
        child: BookGridItem(
          child: book,
        ),
      ),
    );
  }
}
