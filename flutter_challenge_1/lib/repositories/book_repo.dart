import '../models/book_models.dart';

abstract class LibraryStock {
  Future<List<BookModel>> fetchLibraryStock();
}
