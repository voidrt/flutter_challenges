import 'package:flutter/material.dart';
import 'package:flutter_challenge_1/bindings_and_controllers/books_controller.dart';
import 'package:flutter_challenge_1/widgets/book_grid_view.dart/book_item.dart';
import 'package:get/get.dart';

class BookGridView extends GetView<BookController> {
  const BookGridView({Key? key, required this.constraints}) : super(key: key);

  final BoxConstraints constraints;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: controller.obx(
        (state) {
          return CustomScrollView(
            slivers: [
              const SliverAppBar(
                title: Text('Books'),
                centerTitle: false,
                floating: true,
              ),
              SliverGrid(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  childAspectRatio: 4 / 5,
                  mainAxisExtent: constraints.maxHeight * 0.25,
                  crossAxisSpacing: 4,
                ),
                delegate: SliverChildBuilderDelegate(
                  ((_, index) {
                    final singleBook = state[index];
                    return GestureDetector(
                      onTap: () {},
                      child: BookItem(book: singleBook),
                    );
                  }),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
