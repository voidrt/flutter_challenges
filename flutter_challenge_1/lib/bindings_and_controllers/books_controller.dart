import 'package:flutter_challenge_1/repositories/library_stock.dart';
import 'package:get/get.dart';

class BookController extends GetxController with StateMixin {
  BookController(this._libraryStock);

  final GenerateLibraryStock _libraryStock;

  @override
  void onInit() {
    getLibrary();
    super.onInit();
  }

  void getLibrary() async {
    change([], status: RxStatus.loading());
    try {
      final library = await _libraryStock.fetchLibraryStock();
      change(library, status: RxStatus.success());
    } catch (e) {
      change([], status: RxStatus.error('Couldn\'t refresh the page:\n $e'));
    }
  }
}
